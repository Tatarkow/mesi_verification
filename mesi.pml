#define PROCESSOR_COUNT 2

#define RAM_SIZE 4
#define CACHE_SIZE 2

#define UNDEFINED 0
#define READ 1
#define WRITE 2

#define INVALID 0
#define EXCLUSIVE 1
#define SHARED 2
#define MODIFIED 3

#define FALSE 0
#define TRUE 1

bit ram[RAM_SIZE];
bit cache[CACHE_SIZE*PROCESSOR_COUNT];

bit initialized[PROCESSOR_COUNT];
byte cache_line_indices[PROCESSOR_COUNT];
byte states[PROCESSOR_COUNT];
byte buses[PROCESSOR_COUNT];
bit consisent[PROCESSOR_COUNT];
bit finished;

proctype processor() {
	byte other_pid = (_pid % 2) + 1;
	states[_pid-1] = INVALID;

	do
	:: cache_line_indices[_pid-1] < RAM_SIZE / CACHE_SIZE - 1 ->
		cache_line_indices[_pid-1]++;
	:: break;
	od;

	initialized[_pid-1] = TRUE;
	
	byte index;
	byte ram_index;
	byte type;
	byte address;
	byte cache_index;

	if
	:: initialized[other_pid-1] == TRUE -> skip;
	fi;

	do
	:: finished = TRUE -> break;
	:: 
		atomic {
			if
			:: cache_line_indices[_pid-1] == cache_line_indices[other_pid-1] ->
				if
				:: buses[_pid-1] == READ && states[_pid-1] == EXCLUSIVE ->
					states[_pid-1] = SHARED;
				:: buses[_pid-1] == READ && states[_pid-1] == MODIFIED ->
					states[_pid-1] = SHARED;
					buses[other_pid-1] = WRITE;
					index = 0;

					do
					:: index < CACHE_SIZE ->
						ram_index = cache_line_indices[_pid-1]*CACHE_SIZE + index;
						ram[ram_index] = cache[index + (_pid-1) * CACHE_SIZE];
						index++;
					:: else -> break;
					od;
					
				:: buses[_pid-1] == WRITE && states[_pid-1] == EXCLUSIVE ->
					states[_pid-1] = INVALID;
				:: buses[_pid-1] == WRITE && states[_pid-1] == SHARED ->
					states[_pid-1] = INVALID;
				:: buses[_pid-1] == WRITE && states[_pid-1] == MODIFIED ->
					states[_pid-1] = INVALID;
				:: else -> skip;
				fi;
			:: else ->
				if
				:: states[_pid-1] == SHARED -> states[_pid-1] = EXCLUSIVE;
				// Else would be an unrechable state.
				fi;
			fi;

			buses[_pid-1] = UNDEFINED;
			consisent[_pid-1] = TRUE;
		}

		if
		:: type = READ;
		:: type = WRITE;
		fi;

		do
		:: address < RAM_SIZE - 1 -> address++;
		:: break;
		od;	

		if
		:: cache_line_indices[_pid-1] == address / CACHE_SIZE -> skip;
		:: else ->
			atomic {
				states[_pid-1] = INVALID;
				cache_line_indices[_pid-1] = address / CACHE_SIZE;
				consisent[other_pid-1] = FALSE;
			}
		fi;
		
		cache_index = (address % CACHE_SIZE) + (_pid-1) * CACHE_SIZE;

		if
		:: states[_pid-1] == INVALID && type == READ ->
			atomic {
				if
				:: cache_line_indices[_pid-1] == cache_line_indices[other_pid-1] ->
					if
					:: states[other_pid-1] == INVALID -> skip;
					:: states[other_pid-1] == EXCLUSIVE -> buses[other_pid-1] = READ;
					:: states[other_pid-1] == SHARED -> skip;
					:: states[other_pid-1] == MODIFIED ->
						buses[other_pid-1] = READ;
						
						if
						:: buses[other_pid-1] == UNDEFINED || finished == TRUE -> skip;
						fi;
					fi;
					
					states[_pid-1] = SHARED;
				:: else ->
					states[_pid-1] = EXCLUSIVE;
				fi;

				index = 0;

				do
				:: index < CACHE_SIZE ->
					ram_index = cache_line_indices[_pid-1]*CACHE_SIZE + index;
					cache[index + (_pid-1) * CACHE_SIZE] = ram[ram_index];
					index++;
				:: else -> break;
				od;
			}
		:: states[_pid-1] == INVALID && type == WRITE ->
			atomic {
				if
				:: cache_line_indices[_pid-1] == cache_line_indices[other_pid-1] ->
					if
					:: states[other_pid-1] == INVALID -> skip;
					:: states[other_pid-1] == EXCLUSIVE -> buses[other_pid-1] = WRITE;
					:: states[other_pid-1] == SHARED -> buses[other_pid-1] = WRITE;
					:: states[other_pid-1] == MODIFIED ->
						buses[other_pid-1] = WRITE;
						index = 0;

						do
						:: index < CACHE_SIZE ->
							ram_index = cache_line_indices[other_pid-1]*CACHE_SIZE + index;
							ram[ram_index] = cache[index + (other_pid-1) * CACHE_SIZE];
							index++;
						:: else -> break;
						od;
					fi;
				:: else -> skip;
				fi;				

				if
				:: cache[cache_index] = 0;
				:: cache[cache_index] = 1;
				fi;

				states[_pid-1] = MODIFIED;
			}
		:: states[_pid-1] == EXCLUSIVE && type == READ ->
			skip;
		:: states[_pid-1] == EXCLUSIVE && type == WRITE ->
			atomic {
				if
				:: cache[cache_index] = 0;
				:: cache[cache_index] = 1;
				fi;

				states[_pid-1] = MODIFIED;
			}
		:: states[_pid-1] == SHARED && type == READ ->
			skip;
		:: states[_pid-1] == SHARED && type == WRITE ->
			atomic {
				if
				:: cache_line_indices[_pid-1] == cache_line_indices[other_pid-1] ->
					buses[other_pid-1] = WRITE;
				:: else -> skip;
				fi;

				if
				:: cache[cache_index] = 0;
				:: cache[cache_index] = 1;
				fi;

				states[_pid-1] = MODIFIED;
			}
		:: states[_pid-1] == MODIFIED && type == READ ->
			skip;
		:: states[_pid-1] == MODIFIED && type == WRITE ->
			atomic {
				if
				:: cache[cache_index] = 0;
				:: cache[cache_index] = 1;
				fi;
			}
		fi;
	od;
}

proctype monitor() {
	byte monitor_index;

	atomic {
		if
		:: buses[0] == UNDEFINED && buses[1] == UNDEFINED && cache_line_indices[0] == cache_line_indices[1] ->
			assert(!(states[0] == MODIFIED && states[1] == MODIFIED));
			assert(!(states[0] == MODIFIED && states[1] == EXCLUSIVE));
			assert(!(states[0] == MODIFIED && states[1] == SHARED));
			assert(!(states[0] == EXCLUSIVE && states[1] == MODIFIED));
			assert(!(states[0] == EXCLUSIVE && states[1] == EXCLUSIVE));
			assert(!(states[0] == EXCLUSIVE && states[1] == SHARED));
			assert(!(states[0] == SHARED && states[1] == MODIFIED));
			assert(!(states[0] == SHARED && states[1] == EXCLUSIVE));
		:: else -> skip;
		fi;
		
		do
		:: monitor_index < CACHE_SIZE - 1 -> monitor_index++;
		:: break;
		od;

		if 
		:: buses[0] == UNDEFINED && buses[1] == UNDEFINED && consisent[0] == TRUE && consisent[1] == TRUE ->
			assert(!(states[0] == EXCLUSIVE && cache[0*CACHE_SIZE + monitor_index] != ram[cache_line_indices[0]*CACHE_SIZE + monitor_index]));
			assert(!(states[0] == SHARED && cache[0*CACHE_SIZE + monitor_index] != ram[cache_line_indices[0]*CACHE_SIZE + monitor_index]));
			assert(!(states[1] == EXCLUSIVE && cache[1*CACHE_SIZE + monitor_index] != ram[cache_line_indices[1]*CACHE_SIZE + monitor_index]));
			assert(!(states[1] == SHARED && cache[1*CACHE_SIZE + monitor_index] != ram[cache_line_indices[1]*CACHE_SIZE + monitor_index]));
				
			assert(!(states[0] == SHARED && cache_line_indices[0] != cache_line_indices[1]));
			assert(!(states[1] == SHARED && cache_line_indices[0] != cache_line_indices[1]));
		:: else -> skip;
		fi;
	}		
}

init {
	run processor();
	run processor();
	run monitor();
}
