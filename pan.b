	switch (t->back) {
	default: Uerror("bad return move");
	case  0: goto R999; /* nothing to undo */

		 /* PROC :init: */

	case 3: // STATE 1
		;
		;
		delproc(0, now._nr_pr-1);
		;
		goto R999;

	case 4: // STATE 2
		;
		;
		delproc(0, now._nr_pr-1);
		;
		goto R999;

	case 5: // STATE 3
		;
		;
		delproc(0, now._nr_pr-1);
		;
		goto R999;

	case 6: // STATE 4
		;
		p_restor(II);
		;
		;
		goto R999;

		 /* PROC monitor */
;
		
	case 7: // STATE 1
		goto R999;

	case 8: // STATE 15
		;
		((P1 *)_this)->monitor_index = trpt->bup.oval;
		;
		goto R999;
;
		
	case 9: // STATE 20
		goto R999;
;
		
	case 10: // STATE 30
		goto R999;
;
		
	case 11: // STATE 28
		goto R999;

	case 12: // STATE 32
		;
		p_restor(II);
		;
		;
		goto R999;

		 /* PROC processor */

	case 13: // STATE 1
		;
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.oval;
		;
		goto R999;
;
		;
		
	case 15: // STATE 3
		;
		now.cache_line_indices[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.oval;
		;
		goto R999;

	case 16: // STATE 8
		;
		now.initialized[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.oval;
		;
		goto R999;

	case 17: // STATE 13
		;
		((P0 *)_this)->cache_index = trpt->bup.ovals[4];
		((P0 *)_this)->address = trpt->bup.ovals[3];
		((P0 *)_this)->type = trpt->bup.ovals[2];
		((P0 *)_this)->ram_index = trpt->bup.ovals[1];
		((P0 *)_this)->index = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 5);
		goto R999;
;
		;
		
	case 19: // STATE 18
		;
		now.finished = trpt->bup.oval;
		;
		goto R999;
;
		;
		
	case 21: // STATE 54
		;
		now.consisent[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[2];
		now.buses[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[1];
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 3);
		goto R999;

	case 22: // STATE 26
		;
		((P0 *)_this)->index = trpt->bup.ovals[2];
		now.buses[ Index((((P0 *)_this)->other_pid-1), 2) ] = trpt->bup.ovals[1];
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 3);
		goto R999;

	case 23: // STATE 30
		;
		((P0 *)_this)->index = trpt->bup.ovals[2];
		now.ram[ Index(((P0 *)_this)->ram_index, 4) ] = trpt->bup.ovals[1];
		((P0 *)_this)->ram_index = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 3);
		goto R999;

	case 24: // STATE 54
		;
		now.consisent[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[2];
		now.buses[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[1];
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 3);
		goto R999;

	case 25: // STATE 54
		;
		now.consisent[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[2];
		now.buses[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[1];
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 3);
		goto R999;

	case 26: // STATE 54
		;
		now.consisent[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[2];
		now.buses[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[1];
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 3);
		goto R999;

	case 27: // STATE 54
		;
		now.consisent[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[1];
		now.buses[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 2);
		goto R999;

	case 28: // STATE 54
		;
		now.consisent[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[2];
		now.buses[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[1];
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 3);
		goto R999;

	case 29: // STATE 54
		;
		now.consisent[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[1];
		now.buses[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 2);
		goto R999;

	case 30: // STATE 56
		;
		((P0 *)_this)->type = trpt->bup.oval;
		;
		goto R999;

	case 31: // STATE 57
		;
		((P0 *)_this)->type = trpt->bup.oval;
		;
		goto R999;

	case 32: // STATE 61
		;
		((P0 *)_this)->address = trpt->bup.oval;
		;
		goto R999;
;
		;
		
	case 34: // STATE 75
		;
		((P0 *)_this)->cache_index = trpt->bup.oval;
		;
		goto R999;

	case 35: // STATE 75
		;
		((P0 *)_this)->cache_index = trpt->bup.ovals[3];
		now.consisent[ Index((((P0 *)_this)->other_pid-1), 2) ] = trpt->bup.ovals[2];
		now.cache_line_indices[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[1];
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 4);
		goto R999;

	case 36: // STATE 75
		;
		((P0 *)_this)->cache_index = trpt->bup.oval;
		;
		goto R999;

	case 37: // STATE 76
		;
	/* 0 */	((P0 *)_this)->type = trpt->bup.oval;
		;
		;
		goto R999;
;
		;
		;
		;
		
	case 40: // STATE 97
		;
		((P0 *)_this)->index = trpt->bup.ovals[1];
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 2);
		goto R999;

	case 41: // STATE 97
		;
		((P0 *)_this)->index = trpt->bup.ovals[2];
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[1];
		now.buses[ Index((((P0 *)_this)->other_pid-1), 2) ] = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 3);
		goto R999;
;
		;
		
	case 43: // STATE 97
		;
		((P0 *)_this)->index = trpt->bup.ovals[1];
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 2);
		goto R999;

	case 44: // STATE 85
		;
		now.buses[ Index((((P0 *)_this)->other_pid-1), 2) ] = trpt->bup.oval;
		;
		goto R999;
;
		;
		
	case 46: // STATE 97
		;
		((P0 *)_this)->index = trpt->bup.ovals[1];
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 2);
		goto R999;

	case 47: // STATE 97
		;
		((P0 *)_this)->index = trpt->bup.ovals[1];
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 2);
		goto R999;

	case 48: // STATE 97
		;
		((P0 *)_this)->index = trpt->bup.ovals[1];
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 2);
		goto R999;

	case 49: // STATE 97
		;
		((P0 *)_this)->index = trpt->bup.oval;
		;
		goto R999;

	case 50: // STATE 101
		;
		((P0 *)_this)->index = trpt->bup.ovals[2];
		now.cache[ Index((((P0 *)_this)->index+((((P0 *)_this)->_pid-1)*2)), 4) ] = trpt->bup.ovals[1];
		((P0 *)_this)->ram_index = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 3);
		goto R999;

	case 51: // STATE 108
		;
	/* 0 */	((P0 *)_this)->type = trpt->bup.oval;
		;
		;
		goto R999;
;
		;
		;
		;
		
	case 54: // STATE 113
		;
		now.buses[ Index((((P0 *)_this)->other_pid-1), 2) ] = trpt->bup.oval;
		;
		goto R999;

	case 55: // STATE 115
		;
		now.buses[ Index((((P0 *)_this)->other_pid-1), 2) ] = trpt->bup.oval;
		;
		goto R999;

	case 56: // STATE 118
		;
		((P0 *)_this)->index = trpt->bup.ovals[1];
		now.buses[ Index((((P0 *)_this)->other_pid-1), 2) ] = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 2);
		goto R999;

	case 57: // STATE 122
		;
		((P0 *)_this)->index = trpt->bup.ovals[2];
		now.ram[ Index(((P0 *)_this)->ram_index, 4) ] = trpt->bup.ovals[1];
		((P0 *)_this)->ram_index = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 3);
		goto R999;

	case 58: // STATE 138
		;
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[1];
		now.cache[ Index(((P0 *)_this)->cache_index, 4) ] = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 2);
		goto R999;

	case 59: // STATE 138
		;
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.oval;
		;
		goto R999;

	case 60: // STATE 138
		;
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[1];
		now.cache[ Index(((P0 *)_this)->cache_index, 4) ] = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 2);
		goto R999;

	case 61: // STATE 140
		;
	/* 0 */	((P0 *)_this)->type = trpt->bup.oval;
		;
		;
		goto R999;

	case 62: // STATE 142
		;
	/* 0 */	((P0 *)_this)->type = trpt->bup.oval;
		;
		;
		goto R999;

	case 63: // STATE 147
		;
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[1];
		now.cache[ Index(((P0 *)_this)->cache_index, 4) ] = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 2);
		goto R999;

	case 64: // STATE 147
		;
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.oval;
		;
		goto R999;

	case 65: // STATE 147
		;
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[1];
		now.cache[ Index(((P0 *)_this)->cache_index, 4) ] = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 2);
		goto R999;

	case 66: // STATE 149
		;
	/* 0 */	((P0 *)_this)->type = trpt->bup.oval;
		;
		;
		goto R999;

	case 67: // STATE 151
		;
	/* 0 */	((P0 *)_this)->type = trpt->bup.oval;
		;
		;
		goto R999;

	case 68: // STATE 153
		;
		now.buses[ Index((((P0 *)_this)->other_pid-1), 2) ] = trpt->bup.oval;
		;
		goto R999;

	case 69: // STATE 162
		;
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[1];
		now.cache[ Index(((P0 *)_this)->cache_index, 4) ] = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 2);
		goto R999;

	case 70: // STATE 162
		;
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.oval;
		;
		goto R999;

	case 71: // STATE 162
		;
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = trpt->bup.ovals[1];
		now.cache[ Index(((P0 *)_this)->cache_index, 4) ] = trpt->bup.ovals[0];
		;
		ungrab_ints(trpt->bup.ovals, 2);
		goto R999;

	case 72: // STATE 164
		;
	/* 0 */	((P0 *)_this)->type = trpt->bup.oval;
		;
		;
		goto R999;

	case 73: // STATE 166
		;
	/* 0 */	((P0 *)_this)->type = trpt->bup.oval;
		;
		;
		goto R999;

	case 74: // STATE 167
		;
		now.cache[ Index(((P0 *)_this)->cache_index, 4) ] = trpt->bup.oval;
		;
		goto R999;
;
		
	case 75: // STATE 170
		goto R999;

	case 76: // STATE 168
		;
		now.cache[ Index(((P0 *)_this)->cache_index, 4) ] = trpt->bup.oval;
		;
		goto R999;

	case 77: // STATE 177
		;
		p_restor(II);
		;
		;
		goto R999;
	}

