MESI verification
=================

File `mesi.pml` contains model of MESI protocol written in Promela. Using
```
spin -a mesi.pml
```
files `pan.*` can be generated. It is then possible to compile it by running
```
gcc -w -o pan pan.c
```
Finally, executable file `pan` should be generated, and thus we can verify the MESI protocol model by running
```
./pan
```
It may consume up to 5 GB of memory and run couple minutes.