#define rand	pan_rand
#define pthread_equal(a,b)	((a)==(b))
#if defined(HAS_CODE) && defined(VERBOSE)
	#ifdef BFS_PAR
		bfs_printf("Pr: %d Tr: %d\n", II, t->forw);
	#else
		cpu_printf("Pr: %d Tr: %d\n", II, t->forw);
	#endif
#endif
	switch (t->forw) {
	default: Uerror("bad forward move");
	case 0:	/* if without executable clauses */
		continue;
	case 1: /* generic 'goto' or 'skip' */
		IfNotBlocked
		_m = 3; goto P999;
	case 2: /* generic 'else' */
		IfNotBlocked
		if (trpt->o_pm&1) continue;
		_m = 3; goto P999;

		 /* PROC :init: */
	case 3: // STATE 1 - mesi.pml:253 - [(run processor())] (0:0:0 - 1)
		IfNotBlocked
		reached[2][1] = 1;
		if (!(addproc(II, 1, 0)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 4: // STATE 2 - mesi.pml:254 - [(run processor())] (0:0:0 - 1)
		IfNotBlocked
		reached[2][2] = 1;
		if (!(addproc(II, 1, 0)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 5: // STATE 3 - mesi.pml:255 - [(run monitor())] (0:0:0 - 1)
		IfNotBlocked
		reached[2][3] = 1;
		if (!(addproc(II, 1, 1)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 6: // STATE 4 - mesi.pml:256 - [-end-] (0:0:0 - 1)
		IfNotBlocked
		reached[2][4] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC monitor */
	case 7: // STATE 1 - mesi.pml:221 - [((((buses[0]==0)&&(buses[1]==0))&&(cache_line_indices[0]==cache_line_indices[1])))] (17:0:0 - 1)
		IfNotBlocked
		reached[1][1] = 1;
		if (!((((((int)now.buses[0])==0)&&(((int)now.buses[1])==0))&&(((int)now.cache_line_indices[0])==((int)now.cache_line_indices[1])))))
			continue;
		/* merge: assert(!(((states[0]==3)&&(states[1]==3))))(17, 2, 17) */
		reached[1][2] = 1;
		spin_assert( !(((((int)now.states[0])==3)&&(((int)now.states[1])==3))), " !(((states[0]==3)&&(states[1]==3)))", II, tt, t);
		/* merge: assert(!(((states[0]==3)&&(states[1]==1))))(17, 3, 17) */
		reached[1][3] = 1;
		spin_assert( !(((((int)now.states[0])==3)&&(((int)now.states[1])==1))), " !(((states[0]==3)&&(states[1]==1)))", II, tt, t);
		/* merge: assert(!(((states[0]==3)&&(states[1]==2))))(17, 4, 17) */
		reached[1][4] = 1;
		spin_assert( !(((((int)now.states[0])==3)&&(((int)now.states[1])==2))), " !(((states[0]==3)&&(states[1]==2)))", II, tt, t);
		/* merge: assert(!(((states[0]==1)&&(states[1]==3))))(17, 5, 17) */
		reached[1][5] = 1;
		spin_assert( !(((((int)now.states[0])==1)&&(((int)now.states[1])==3))), " !(((states[0]==1)&&(states[1]==3)))", II, tt, t);
		/* merge: assert(!(((states[0]==1)&&(states[1]==1))))(17, 6, 17) */
		reached[1][6] = 1;
		spin_assert( !(((((int)now.states[0])==1)&&(((int)now.states[1])==1))), " !(((states[0]==1)&&(states[1]==1)))", II, tt, t);
		/* merge: assert(!(((states[0]==1)&&(states[1]==2))))(17, 7, 17) */
		reached[1][7] = 1;
		spin_assert( !(((((int)now.states[0])==1)&&(((int)now.states[1])==2))), " !(((states[0]==1)&&(states[1]==2)))", II, tt, t);
		/* merge: assert(!(((states[0]==2)&&(states[1]==3))))(17, 8, 17) */
		reached[1][8] = 1;
		spin_assert( !(((((int)now.states[0])==2)&&(((int)now.states[1])==3))), " !(((states[0]==2)&&(states[1]==3)))", II, tt, t);
		/* merge: assert(!(((states[0]==2)&&(states[1]==1))))(17, 9, 17) */
		reached[1][9] = 1;
		spin_assert( !(((((int)now.states[0])==2)&&(((int)now.states[1])==1))), " !(((states[0]==2)&&(states[1]==1)))", II, tt, t);
		/* merge: .(goto)(0, 13, 17) */
		reached[1][13] = 1;
		;
		/* merge: .(goto)(0, 18, 17) */
		reached[1][18] = 1;
		;
		_m = 3; goto P999; /* 10 */
	case 8: // STATE 14 - mesi.pml:234 - [((monitor_index<(2-1)))] (17:0:1 - 1)
		IfNotBlocked
		reached[1][14] = 1;
		if (!((((int)((P1 *)_this)->monitor_index)<(2-1))))
			continue;
		/* merge: monitor_index = (monitor_index+1)(0, 15, 17) */
		reached[1][15] = 1;
		(trpt+1)->bup.oval = ((int)((P1 *)_this)->monitor_index);
		((P1 *)_this)->monitor_index = (((int)((P1 *)_this)->monitor_index)+1);
#ifdef VAR_RANGES
		logval("monitor:monitor_index", ((int)((P1 *)_this)->monitor_index));
#endif
		;
		/* merge: .(goto)(0, 18, 17) */
		reached[1][18] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 9: // STATE 20 - mesi.pml:239 - [(((((buses[0]==0)&&(buses[1]==0))&&(consisent[0]==1))&&(consisent[1]==1)))] (32:0:0 - 1)
		IfNotBlocked
		reached[1][20] = 1;
		if (!(((((((int)now.buses[0])==0)&&(((int)now.buses[1])==0))&&(((int)now.consisent[0])==1))&&(((int)now.consisent[1])==1))))
			continue;
		/* merge: assert(!(((states[0]==1)&&(cache[((0*2)+monitor_index)]!=ram[((cache_line_indices[0]*2)+monitor_index)]))))(32, 21, 32) */
		reached[1][21] = 1;
		spin_assert( !(((((int)now.states[0])==1)&&(((int)now.cache[ Index(((0*2)+((int)((P1 *)_this)->monitor_index)), 4) ])!=((int)now.ram[ Index(((((int)now.cache_line_indices[0])*2)+((int)((P1 *)_this)->monitor_index)), 4) ])))), " !(((states[0]==1)&&(cache[((0*2)+monitor_index)]!=ram[((cache_line_indices[0]*2)+monitor_index)])))", II, tt, t);
		/* merge: assert(!(((states[0]==2)&&(cache[((0*2)+monitor_index)]!=ram[((cache_line_indices[0]*2)+monitor_index)]))))(32, 22, 32) */
		reached[1][22] = 1;
		spin_assert( !(((((int)now.states[0])==2)&&(((int)now.cache[ Index(((0*2)+((int)((P1 *)_this)->monitor_index)), 4) ])!=((int)now.ram[ Index(((((int)now.cache_line_indices[0])*2)+((int)((P1 *)_this)->monitor_index)), 4) ])))), " !(((states[0]==2)&&(cache[((0*2)+monitor_index)]!=ram[((cache_line_indices[0]*2)+monitor_index)])))", II, tt, t);
		/* merge: assert(!(((states[1]==1)&&(cache[((1*2)+monitor_index)]!=ram[((cache_line_indices[1]*2)+monitor_index)]))))(32, 23, 32) */
		reached[1][23] = 1;
		spin_assert( !(((((int)now.states[1])==1)&&(((int)now.cache[ Index(((1*2)+((int)((P1 *)_this)->monitor_index)), 4) ])!=((int)now.ram[ Index(((((int)now.cache_line_indices[1])*2)+((int)((P1 *)_this)->monitor_index)), 4) ])))), " !(((states[1]==1)&&(cache[((1*2)+monitor_index)]!=ram[((cache_line_indices[1]*2)+monitor_index)])))", II, tt, t);
		/* merge: assert(!(((states[1]==2)&&(cache[((1*2)+monitor_index)]!=ram[((cache_line_indices[1]*2)+monitor_index)]))))(32, 24, 32) */
		reached[1][24] = 1;
		spin_assert( !(((((int)now.states[1])==2)&&(((int)now.cache[ Index(((1*2)+((int)((P1 *)_this)->monitor_index)), 4) ])!=((int)now.ram[ Index(((((int)now.cache_line_indices[1])*2)+((int)((P1 *)_this)->monitor_index)), 4) ])))), " !(((states[1]==2)&&(cache[((1*2)+monitor_index)]!=ram[((cache_line_indices[1]*2)+monitor_index)])))", II, tt, t);
		/* merge: assert(!(((states[0]==2)&&(cache_line_indices[0]!=cache_line_indices[1]))))(32, 25, 32) */
		reached[1][25] = 1;
		spin_assert( !(((((int)now.states[0])==2)&&(((int)now.cache_line_indices[0])!=((int)now.cache_line_indices[1])))), " !(((states[0]==2)&&(cache_line_indices[0]!=cache_line_indices[1])))", II, tt, t);
		/* merge: assert(!(((states[1]==2)&&(cache_line_indices[0]!=cache_line_indices[1]))))(32, 26, 32) */
		reached[1][26] = 1;
		spin_assert( !(((((int)now.states[1])==2)&&(((int)now.cache_line_indices[0])!=((int)now.cache_line_indices[1])))), " !(((states[1]==2)&&(cache_line_indices[0]!=cache_line_indices[1])))", II, tt, t);
		/* merge: .(goto)(32, 30, 32) */
		reached[1][30] = 1;
		;
		_m = 3; goto P999; /* 7 */
	case 10: // STATE 30 - mesi.pml:249 - [.(goto)] (0:32:0 - 2)
		IfNotBlocked
		reached[1][30] = 1;
		;
		_m = 3; goto P999; /* 0 */
	case 11: // STATE 28 - mesi.pml:247 - [(1)] (32:0:0 - 1)
		IfNotBlocked
		reached[1][28] = 1;
		if (!(1))
			continue;
		/* merge: .(goto)(32, 30, 32) */
		reached[1][30] = 1;
		;
		_m = 3; goto P999; /* 1 */
	case 12: // STATE 32 - mesi.pml:250 - [-end-] (0:0:0 - 1)
		IfNotBlocked
		reached[1][32] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC processor */
	case 13: // STATE 1 - mesi.pml:30 - [states[(_pid-1)] = 0] (0:0:1 - 1)
		IfNotBlocked
		reached[0][1] = 1;
		(trpt+1)->bup.oval = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 0;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 14: // STATE 2 - mesi.pml:33 - [((cache_line_indices[(_pid-1)]<((4/2)-1)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][2] = 1;
		if (!((((int)now.cache_line_indices[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])<((4/2)-1))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 15: // STATE 3 - mesi.pml:34 - [cache_line_indices[(_pid-1)] = (cache_line_indices[(_pid-1)]+1)] (0:0:1 - 1)
		IfNotBlocked
		reached[0][3] = 1;
		(trpt+1)->bup.oval = ((int)now.cache_line_indices[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.cache_line_indices[ Index((((P0 *)_this)->_pid-1), 2) ] = (((int)now.cache_line_indices[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])+1);
#ifdef VAR_RANGES
		logval("cache_line_indices[(_pid-1)]", ((int)now.cache_line_indices[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 16: // STATE 8 - mesi.pml:38 - [initialized[(_pid-1)] = 1] (0:0:1 - 2)
		IfNotBlocked
		reached[0][8] = 1;
		(trpt+1)->bup.oval = ((int)now.initialized[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.initialized[ Index((((P0 *)_this)->_pid-1), 2) ] = 1;
#ifdef VAR_RANGES
		logval("initialized[(_pid-1)]", ((int)now.initialized[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 17: // STATE 9 - mesi.pml:41 - [index = 0] (0:16:5 - 1)
		IfNotBlocked
		reached[0][9] = 1;
		(trpt+1)->bup.ovals = grab_ints(5);
		(trpt+1)->bup.ovals[0] = ((int)((P0 *)_this)->index);
		((P0 *)_this)->index = 0;
#ifdef VAR_RANGES
		logval("processor:index", ((int)((P0 *)_this)->index));
#endif
		;
		/* merge: ram_index = 0(16, 10, 16) */
		reached[0][10] = 1;
		(trpt+1)->bup.ovals[1] = ((int)((P0 *)_this)->ram_index);
		((P0 *)_this)->ram_index = 0;
#ifdef VAR_RANGES
		logval("processor:ram_index", ((int)((P0 *)_this)->ram_index));
#endif
		;
		/* merge: type = 0(16, 11, 16) */
		reached[0][11] = 1;
		(trpt+1)->bup.ovals[2] = ((int)((P0 *)_this)->type);
		((P0 *)_this)->type = 0;
#ifdef VAR_RANGES
		logval("processor:type", ((int)((P0 *)_this)->type));
#endif
		;
		/* merge: address = 0(16, 12, 16) */
		reached[0][12] = 1;
		(trpt+1)->bup.ovals[3] = ((int)((P0 *)_this)->address);
		((P0 *)_this)->address = 0;
#ifdef VAR_RANGES
		logval("processor:address", ((int)((P0 *)_this)->address));
#endif
		;
		/* merge: cache_index = 0(16, 13, 16) */
		reached[0][13] = 1;
		(trpt+1)->bup.ovals[4] = ((int)((P0 *)_this)->cache_index);
		((P0 *)_this)->cache_index = 0;
#ifdef VAR_RANGES
		logval("processor:cache_index", ((int)((P0 *)_this)->cache_index));
#endif
		;
		_m = 3; goto P999; /* 4 */
	case 18: // STATE 14 - mesi.pml:47 - [((initialized[(other_pid-1)]==1))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][14] = 1;
		if (!((((int)now.initialized[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ])==1)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 19: // STATE 18 - mesi.pml:51 - [finished = 1] (0:0:1 - 1)
		IfNotBlocked
		reached[0][18] = 1;
		(trpt+1)->bup.oval = ((int)now.finished);
		now.finished = 1;
#ifdef VAR_RANGES
		logval("finished", ((int)now.finished));
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 20: // STATE 20 - mesi.pml:55 - [((cache_line_indices[(_pid-1)]==cache_line_indices[(other_pid-1)]))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][20] = 1;
		if (!((((int)now.cache_line_indices[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==((int)now.cache_line_indices[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ]))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 21: // STATE 21 - mesi.pml:57 - [(((buses[(_pid-1)]==1)&&(states[(_pid-1)]==1)))] (58:0:3 - 1)
		IfNotBlocked
		reached[0][21] = 1;
		if (!(((((int)now.buses[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==1)&&(((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==1))))
			continue;
		/* merge: states[(_pid-1)] = 2(58, 22, 58) */
		reached[0][22] = 1;
		(trpt+1)->bup.ovals = grab_ints(3);
		(trpt+1)->bup.ovals[0] = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 2;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(58, 45, 58) */
		reached[0][45] = 1;
		;
		/* merge: .(goto)(58, 52, 58) */
		reached[0][52] = 1;
		;
		/* merge: buses[(_pid-1)] = 0(58, 53, 58) */
		reached[0][53] = 1;
		(trpt+1)->bup.ovals[1] = ((int)now.buses[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.buses[ Index((((P0 *)_this)->_pid-1), 2) ] = 0;
#ifdef VAR_RANGES
		logval("buses[(_pid-1)]", ((int)now.buses[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: consisent[(_pid-1)] = 1(58, 54, 58) */
		reached[0][54] = 1;
		(trpt+1)->bup.ovals[2] = ((int)now.consisent[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.consisent[ Index((((P0 *)_this)->_pid-1), 2) ] = 1;
#ifdef VAR_RANGES
		logval("consisent[(_pid-1)]", ((int)now.consisent[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		_m = 3; goto P999; /* 5 */
	case 22: // STATE 23 - mesi.pml:59 - [(((buses[(_pid-1)]==1)&&(states[(_pid-1)]==3)))] (33:0:3 - 1)
		IfNotBlocked
		reached[0][23] = 1;
		if (!(((((int)now.buses[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==1)&&(((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==3))))
			continue;
		/* merge: states[(_pid-1)] = 2(33, 24, 33) */
		reached[0][24] = 1;
		(trpt+1)->bup.ovals = grab_ints(3);
		(trpt+1)->bup.ovals[0] = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 2;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: buses[(other_pid-1)] = 2(33, 25, 33) */
		reached[0][25] = 1;
		(trpt+1)->bup.ovals[1] = ((int)now.buses[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ]);
		now.buses[ Index((((P0 *)_this)->other_pid-1), 2) ] = 2;
#ifdef VAR_RANGES
		logval("buses[(processor:other_pid-1)]", ((int)now.buses[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ]));
#endif
		;
		/* merge: index = 0(33, 26, 33) */
		reached[0][26] = 1;
		(trpt+1)->bup.ovals[2] = ((int)((P0 *)_this)->index);
		((P0 *)_this)->index = 0;
#ifdef VAR_RANGES
		logval("processor:index", ((int)((P0 *)_this)->index));
#endif
		;
		/* merge: .(goto)(0, 34, 33) */
		reached[0][34] = 1;
		;
		_m = 3; goto P999; /* 4 */
	case 23: // STATE 27 - mesi.pml:65 - [((index<2))] (33:0:3 - 1)
		IfNotBlocked
		reached[0][27] = 1;
		if (!((((int)((P0 *)_this)->index)<2)))
			continue;
		/* merge: ram_index = ((cache_line_indices[(_pid-1)]*2)+index)(33, 28, 33) */
		reached[0][28] = 1;
		(trpt+1)->bup.ovals = grab_ints(3);
		(trpt+1)->bup.ovals[0] = ((int)((P0 *)_this)->ram_index);
		((P0 *)_this)->ram_index = ((((int)now.cache_line_indices[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])*2)+((int)((P0 *)_this)->index));
#ifdef VAR_RANGES
		logval("processor:ram_index", ((int)((P0 *)_this)->ram_index));
#endif
		;
		/* merge: ram[ram_index] = cache[(index+((_pid-1)*2))](33, 29, 33) */
		reached[0][29] = 1;
		(trpt+1)->bup.ovals[1] = ((int)now.ram[ Index(((int)((P0 *)_this)->ram_index), 4) ]);
		now.ram[ Index(((P0 *)_this)->ram_index, 4) ] = ((int)now.cache[ Index((((int)((P0 *)_this)->index)+((((int)((P0 *)_this)->_pid)-1)*2)), 4) ]);
#ifdef VAR_RANGES
		logval("ram[processor:ram_index]", ((int)now.ram[ Index(((int)((P0 *)_this)->ram_index), 4) ]));
#endif
		;
		/* merge: index = (index+1)(33, 30, 33) */
		reached[0][30] = 1;
		(trpt+1)->bup.ovals[2] = ((int)((P0 *)_this)->index);
		((P0 *)_this)->index = (((int)((P0 *)_this)->index)+1);
#ifdef VAR_RANGES
		logval("processor:index", ((int)((P0 *)_this)->index));
#endif
		;
		/* merge: .(goto)(0, 34, 33) */
		reached[0][34] = 1;
		;
		_m = 3; goto P999; /* 4 */
	case 24: // STATE 36 - mesi.pml:72 - [(((buses[(_pid-1)]==2)&&(states[(_pid-1)]==1)))] (58:0:3 - 1)
		IfNotBlocked
		reached[0][36] = 1;
		if (!(((((int)now.buses[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==2)&&(((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==1))))
			continue;
		/* merge: states[(_pid-1)] = 0(58, 37, 58) */
		reached[0][37] = 1;
		(trpt+1)->bup.ovals = grab_ints(3);
		(trpt+1)->bup.ovals[0] = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 0;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(58, 45, 58) */
		reached[0][45] = 1;
		;
		/* merge: .(goto)(58, 52, 58) */
		reached[0][52] = 1;
		;
		/* merge: buses[(_pid-1)] = 0(58, 53, 58) */
		reached[0][53] = 1;
		(trpt+1)->bup.ovals[1] = ((int)now.buses[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.buses[ Index((((P0 *)_this)->_pid-1), 2) ] = 0;
#ifdef VAR_RANGES
		logval("buses[(_pid-1)]", ((int)now.buses[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: consisent[(_pid-1)] = 1(58, 54, 58) */
		reached[0][54] = 1;
		(trpt+1)->bup.ovals[2] = ((int)now.consisent[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.consisent[ Index((((P0 *)_this)->_pid-1), 2) ] = 1;
#ifdef VAR_RANGES
		logval("consisent[(_pid-1)]", ((int)now.consisent[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		_m = 3; goto P999; /* 5 */
	case 25: // STATE 38 - mesi.pml:74 - [(((buses[(_pid-1)]==2)&&(states[(_pid-1)]==2)))] (58:0:3 - 1)
		IfNotBlocked
		reached[0][38] = 1;
		if (!(((((int)now.buses[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==2)&&(((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==2))))
			continue;
		/* merge: states[(_pid-1)] = 0(58, 39, 58) */
		reached[0][39] = 1;
		(trpt+1)->bup.ovals = grab_ints(3);
		(trpt+1)->bup.ovals[0] = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 0;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(58, 45, 58) */
		reached[0][45] = 1;
		;
		/* merge: .(goto)(58, 52, 58) */
		reached[0][52] = 1;
		;
		/* merge: buses[(_pid-1)] = 0(58, 53, 58) */
		reached[0][53] = 1;
		(trpt+1)->bup.ovals[1] = ((int)now.buses[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.buses[ Index((((P0 *)_this)->_pid-1), 2) ] = 0;
#ifdef VAR_RANGES
		logval("buses[(_pid-1)]", ((int)now.buses[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: consisent[(_pid-1)] = 1(58, 54, 58) */
		reached[0][54] = 1;
		(trpt+1)->bup.ovals[2] = ((int)now.consisent[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.consisent[ Index((((P0 *)_this)->_pid-1), 2) ] = 1;
#ifdef VAR_RANGES
		logval("consisent[(_pid-1)]", ((int)now.consisent[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		_m = 3; goto P999; /* 5 */
	case 26: // STATE 40 - mesi.pml:76 - [(((buses[(_pid-1)]==2)&&(states[(_pid-1)]==3)))] (58:0:3 - 1)
		IfNotBlocked
		reached[0][40] = 1;
		if (!(((((int)now.buses[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==2)&&(((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==3))))
			continue;
		/* merge: states[(_pid-1)] = 0(58, 41, 58) */
		reached[0][41] = 1;
		(trpt+1)->bup.ovals = grab_ints(3);
		(trpt+1)->bup.ovals[0] = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 0;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(58, 45, 58) */
		reached[0][45] = 1;
		;
		/* merge: .(goto)(58, 52, 58) */
		reached[0][52] = 1;
		;
		/* merge: buses[(_pid-1)] = 0(58, 53, 58) */
		reached[0][53] = 1;
		(trpt+1)->bup.ovals[1] = ((int)now.buses[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.buses[ Index((((P0 *)_this)->_pid-1), 2) ] = 0;
#ifdef VAR_RANGES
		logval("buses[(_pid-1)]", ((int)now.buses[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: consisent[(_pid-1)] = 1(58, 54, 58) */
		reached[0][54] = 1;
		(trpt+1)->bup.ovals[2] = ((int)now.consisent[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.consisent[ Index((((P0 *)_this)->_pid-1), 2) ] = 1;
#ifdef VAR_RANGES
		logval("consisent[(_pid-1)]", ((int)now.consisent[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		_m = 3; goto P999; /* 5 */
	case 27: // STATE 43 - mesi.pml:78 - [(1)] (58:0:2 - 1)
		IfNotBlocked
		reached[0][43] = 1;
		if (!(1))
			continue;
		/* merge: .(goto)(58, 45, 58) */
		reached[0][45] = 1;
		;
		/* merge: .(goto)(58, 52, 58) */
		reached[0][52] = 1;
		;
		/* merge: buses[(_pid-1)] = 0(58, 53, 58) */
		reached[0][53] = 1;
		(trpt+1)->bup.ovals = grab_ints(2);
		(trpt+1)->bup.ovals[0] = ((int)now.buses[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.buses[ Index((((P0 *)_this)->_pid-1), 2) ] = 0;
#ifdef VAR_RANGES
		logval("buses[(_pid-1)]", ((int)now.buses[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: consisent[(_pid-1)] = 1(58, 54, 58) */
		reached[0][54] = 1;
		(trpt+1)->bup.ovals[1] = ((int)now.consisent[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.consisent[ Index((((P0 *)_this)->_pid-1), 2) ] = 1;
#ifdef VAR_RANGES
		logval("consisent[(_pid-1)]", ((int)now.consisent[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		_m = 3; goto P999; /* 4 */
	case 28: // STATE 47 - mesi.pml:82 - [((states[(_pid-1)]==2))] (58:0:3 - 1)
		IfNotBlocked
		reached[0][47] = 1;
		if (!((((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==2)))
			continue;
		/* merge: states[(_pid-1)] = 1(58, 48, 58) */
		reached[0][48] = 1;
		(trpt+1)->bup.ovals = grab_ints(3);
		(trpt+1)->bup.ovals[0] = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 1;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(58, 50, 58) */
		reached[0][50] = 1;
		;
		/* merge: .(goto)(58, 52, 58) */
		reached[0][52] = 1;
		;
		/* merge: buses[(_pid-1)] = 0(58, 53, 58) */
		reached[0][53] = 1;
		(trpt+1)->bup.ovals[1] = ((int)now.buses[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.buses[ Index((((P0 *)_this)->_pid-1), 2) ] = 0;
#ifdef VAR_RANGES
		logval("buses[(_pid-1)]", ((int)now.buses[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: consisent[(_pid-1)] = 1(58, 54, 58) */
		reached[0][54] = 1;
		(trpt+1)->bup.ovals[2] = ((int)now.consisent[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.consisent[ Index((((P0 *)_this)->_pid-1), 2) ] = 1;
#ifdef VAR_RANGES
		logval("consisent[(_pid-1)]", ((int)now.consisent[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		_m = 3; goto P999; /* 5 */
	case 29: // STATE 53 - mesi.pml:87 - [buses[(_pid-1)] = 0] (0:58:2 - 12)
		IfNotBlocked
		reached[0][53] = 1;
		(trpt+1)->bup.ovals = grab_ints(2);
		(trpt+1)->bup.ovals[0] = ((int)now.buses[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.buses[ Index((((P0 *)_this)->_pid-1), 2) ] = 0;
#ifdef VAR_RANGES
		logval("buses[(_pid-1)]", ((int)now.buses[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: consisent[(_pid-1)] = 1(58, 54, 58) */
		reached[0][54] = 1;
		(trpt+1)->bup.ovals[1] = ((int)now.consisent[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.consisent[ Index((((P0 *)_this)->_pid-1), 2) ] = 1;
#ifdef VAR_RANGES
		logval("consisent[(_pid-1)]", ((int)now.consisent[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		_m = 3; goto P999; /* 1 */
	case 30: // STATE 56 - mesi.pml:92 - [type = 1] (0:0:1 - 1)
		IfNotBlocked
		reached[0][56] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)_this)->type);
		((P0 *)_this)->type = 1;
#ifdef VAR_RANGES
		logval("processor:type", ((int)((P0 *)_this)->type));
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 31: // STATE 57 - mesi.pml:93 - [type = 2] (0:0:1 - 1)
		IfNotBlocked
		reached[0][57] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)_this)->type);
		((P0 *)_this)->type = 2;
#ifdef VAR_RANGES
		logval("processor:type", ((int)((P0 *)_this)->type));
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 32: // STATE 60 - mesi.pml:97 - [((address<(4-1)))] (63:0:1 - 1)
		IfNotBlocked
		reached[0][60] = 1;
		if (!((((int)((P0 *)_this)->address)<(4-1))))
			continue;
		/* merge: address = (address+1)(0, 61, 63) */
		reached[0][61] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)_this)->address);
		((P0 *)_this)->address = (((int)((P0 *)_this)->address)+1);
#ifdef VAR_RANGES
		logval("processor:address", ((int)((P0 *)_this)->address));
#endif
		;
		/* merge: .(goto)(0, 64, 63) */
		reached[0][64] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 33: // STATE 66 - mesi.pml:102 - [((cache_line_indices[(_pid-1)]==(address/2)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][66] = 1;
		if (!((((int)now.cache_line_indices[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==(((int)((P0 *)_this)->address)/2))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 34: // STATE 67 - mesi.pml:102 - [(1)] (172:0:1 - 1)
		IfNotBlocked
		reached[0][67] = 1;
		if (!(1))
			continue;
		/* merge: .(goto)(172, 74, 172) */
		reached[0][74] = 1;
		;
		/* merge: cache_index = ((address%2)+((_pid-1)*2))(172, 75, 172) */
		reached[0][75] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)_this)->cache_index);
		((P0 *)_this)->cache_index = ((((int)((P0 *)_this)->address)%2)+((((int)((P0 *)_this)->_pid)-1)*2));
#ifdef VAR_RANGES
		logval("processor:cache_index", ((int)((P0 *)_this)->cache_index));
#endif
		;
		_m = 3; goto P999; /* 2 */
	case 35: // STATE 69 - mesi.pml:105 - [states[(_pid-1)] = 0] (0:172:4 - 1)
		IfNotBlocked
		reached[0][69] = 1;
		(trpt+1)->bup.ovals = grab_ints(4);
		(trpt+1)->bup.ovals[0] = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 0;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: cache_line_indices[(_pid-1)] = (address/2)(172, 70, 172) */
		reached[0][70] = 1;
		(trpt+1)->bup.ovals[1] = ((int)now.cache_line_indices[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.cache_line_indices[ Index((((P0 *)_this)->_pid-1), 2) ] = (((int)((P0 *)_this)->address)/2);
#ifdef VAR_RANGES
		logval("cache_line_indices[(_pid-1)]", ((int)now.cache_line_indices[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: consisent[(other_pid-1)] = 0(172, 71, 172) */
		reached[0][71] = 1;
		(trpt+1)->bup.ovals[2] = ((int)now.consisent[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ]);
		now.consisent[ Index((((P0 *)_this)->other_pid-1), 2) ] = 0;
#ifdef VAR_RANGES
		logval("consisent[(processor:other_pid-1)]", ((int)now.consisent[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(172, 74, 172) */
		reached[0][74] = 1;
		;
		/* merge: cache_index = ((address%2)+((_pid-1)*2))(172, 75, 172) */
		reached[0][75] = 1;
		(trpt+1)->bup.ovals[3] = ((int)((P0 *)_this)->cache_index);
		((P0 *)_this)->cache_index = ((((int)((P0 *)_this)->address)%2)+((((int)((P0 *)_this)->_pid)-1)*2));
#ifdef VAR_RANGES
		logval("processor:cache_index", ((int)((P0 *)_this)->cache_index));
#endif
		;
		_m = 3; goto P999; /* 4 */
	case 36: // STATE 75 - mesi.pml:111 - [cache_index = ((address%2)+((_pid-1)*2))] (0:172:1 - 3)
		IfNotBlocked
		reached[0][75] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)_this)->cache_index);
		((P0 *)_this)->cache_index = ((((int)((P0 *)_this)->address)%2)+((((int)((P0 *)_this)->_pid)-1)*2));
#ifdef VAR_RANGES
		logval("processor:cache_index", ((int)((P0 *)_this)->cache_index));
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 37: // STATE 76 - mesi.pml:114 - [(((states[(_pid-1)]==0)&&(type==1)))] (0:0:1 - 1)
		IfNotBlocked
		reached[0][76] = 1;
		if (!(((((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==0)&&(((int)((P0 *)_this)->type)==1))))
			continue;
		if (TstOnly) return 1; /* TT */
		/* dead 1: type */  (trpt+1)->bup.oval = ((P0 *)_this)->type;
#ifdef HAS_CODE
		if (!readtrail)
#endif
			((P0 *)_this)->type = 0;
		_m = 3; goto P999; /* 0 */
	case 38: // STATE 77 - mesi.pml:117 - [((cache_line_indices[(_pid-1)]==cache_line_indices[(other_pid-1)]))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][77] = 1;
		if (!((((int)now.cache_line_indices[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==((int)now.cache_line_indices[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ]))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 39: // STATE 78 - mesi.pml:119 - [((states[(other_pid-1)]==0))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][78] = 1;
		if (!((((int)now.states[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ])==0)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 40: // STATE 79 - mesi.pml:119 - [(1)] (104:0:2 - 1)
		IfNotBlocked
		reached[0][79] = 1;
		if (!(1))
			continue;
		/* merge: .(goto)(104, 91, 104) */
		reached[0][91] = 1;
		;
		/* merge: states[(_pid-1)] = 2(104, 92, 104) */
		reached[0][92] = 1;
		(trpt+1)->bup.ovals = grab_ints(2);
		(trpt+1)->bup.ovals[0] = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 2;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(104, 96, 104) */
		reached[0][96] = 1;
		;
		/* merge: index = 0(104, 97, 104) */
		reached[0][97] = 1;
		(trpt+1)->bup.ovals[1] = ((int)((P0 *)_this)->index);
		((P0 *)_this)->index = 0;
#ifdef VAR_RANGES
		logval("processor:index", ((int)((P0 *)_this)->index));
#endif
		;
		/* merge: .(goto)(0, 105, 104) */
		reached[0][105] = 1;
		;
		_m = 3; goto P999; /* 5 */
	case 41: // STATE 80 - mesi.pml:120 - [((states[(other_pid-1)]==1))] (104:0:3 - 1)
		IfNotBlocked
		reached[0][80] = 1;
		if (!((((int)now.states[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ])==1)))
			continue;
		/* merge: buses[(other_pid-1)] = 1(104, 81, 104) */
		reached[0][81] = 1;
		(trpt+1)->bup.ovals = grab_ints(3);
		(trpt+1)->bup.ovals[0] = ((int)now.buses[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ]);
		now.buses[ Index((((P0 *)_this)->other_pid-1), 2) ] = 1;
#ifdef VAR_RANGES
		logval("buses[(processor:other_pid-1)]", ((int)now.buses[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(104, 91, 104) */
		reached[0][91] = 1;
		;
		/* merge: states[(_pid-1)] = 2(104, 92, 104) */
		reached[0][92] = 1;
		(trpt+1)->bup.ovals[1] = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 2;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(104, 96, 104) */
		reached[0][96] = 1;
		;
		/* merge: index = 0(104, 97, 104) */
		reached[0][97] = 1;
		(trpt+1)->bup.ovals[2] = ((int)((P0 *)_this)->index);
		((P0 *)_this)->index = 0;
#ifdef VAR_RANGES
		logval("processor:index", ((int)((P0 *)_this)->index));
#endif
		;
		/* merge: .(goto)(0, 105, 104) */
		reached[0][105] = 1;
		;
		_m = 3; goto P999; /* 6 */
	case 42: // STATE 82 - mesi.pml:121 - [((states[(other_pid-1)]==2))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][82] = 1;
		if (!((((int)now.states[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ])==2)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 43: // STATE 83 - mesi.pml:121 - [(1)] (104:0:2 - 1)
		IfNotBlocked
		reached[0][83] = 1;
		if (!(1))
			continue;
		/* merge: .(goto)(104, 91, 104) */
		reached[0][91] = 1;
		;
		/* merge: states[(_pid-1)] = 2(104, 92, 104) */
		reached[0][92] = 1;
		(trpt+1)->bup.ovals = grab_ints(2);
		(trpt+1)->bup.ovals[0] = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 2;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(104, 96, 104) */
		reached[0][96] = 1;
		;
		/* merge: index = 0(104, 97, 104) */
		reached[0][97] = 1;
		(trpt+1)->bup.ovals[1] = ((int)((P0 *)_this)->index);
		((P0 *)_this)->index = 0;
#ifdef VAR_RANGES
		logval("processor:index", ((int)((P0 *)_this)->index));
#endif
		;
		/* merge: .(goto)(0, 105, 104) */
		reached[0][105] = 1;
		;
		_m = 3; goto P999; /* 5 */
	case 44: // STATE 84 - mesi.pml:122 - [((states[(other_pid-1)]==3))] (88:0:1 - 1)
		IfNotBlocked
		reached[0][84] = 1;
		if (!((((int)now.states[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ])==3)))
			continue;
		/* merge: buses[(other_pid-1)] = 1(0, 85, 88) */
		reached[0][85] = 1;
		(trpt+1)->bup.oval = ((int)now.buses[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ]);
		now.buses[ Index((((P0 *)_this)->other_pid-1), 2) ] = 1;
#ifdef VAR_RANGES
		logval("buses[(processor:other_pid-1)]", ((int)now.buses[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ]));
#endif
		;
		_m = 3; goto P999; /* 1 */
	case 45: // STATE 86 - mesi.pml:126 - [(((buses[(other_pid-1)]==0)||(finished==1)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][86] = 1;
		if (!(((((int)now.buses[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ])==0)||(((int)now.finished)==1))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 46: // STATE 87 - mesi.pml:126 - [(1)] (104:0:2 - 1)
		IfNotBlocked
		reached[0][87] = 1;
		if (!(1))
			continue;
		/* merge: .(goto)(104, 89, 104) */
		reached[0][89] = 1;
		;
		/* merge: .(goto)(104, 91, 104) */
		reached[0][91] = 1;
		;
		/* merge: states[(_pid-1)] = 2(104, 92, 104) */
		reached[0][92] = 1;
		(trpt+1)->bup.ovals = grab_ints(2);
		(trpt+1)->bup.ovals[0] = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 2;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(104, 96, 104) */
		reached[0][96] = 1;
		;
		/* merge: index = 0(104, 97, 104) */
		reached[0][97] = 1;
		(trpt+1)->bup.ovals[1] = ((int)((P0 *)_this)->index);
		((P0 *)_this)->index = 0;
#ifdef VAR_RANGES
		logval("processor:index", ((int)((P0 *)_this)->index));
#endif
		;
		/* merge: .(goto)(0, 105, 104) */
		reached[0][105] = 1;
		;
		_m = 3; goto P999; /* 6 */
	case 47: // STATE 92 - mesi.pml:130 - [states[(_pid-1)] = 2] (0:104:2 - 6)
		IfNotBlocked
		reached[0][92] = 1;
		(trpt+1)->bup.ovals = grab_ints(2);
		(trpt+1)->bup.ovals[0] = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 2;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(104, 96, 104) */
		reached[0][96] = 1;
		;
		/* merge: index = 0(104, 97, 104) */
		reached[0][97] = 1;
		(trpt+1)->bup.ovals[1] = ((int)((P0 *)_this)->index);
		((P0 *)_this)->index = 0;
#ifdef VAR_RANGES
		logval("processor:index", ((int)((P0 *)_this)->index));
#endif
		;
		/* merge: .(goto)(0, 105, 104) */
		reached[0][105] = 1;
		;
		_m = 3; goto P999; /* 3 */
	case 48: // STATE 94 - mesi.pml:132 - [states[(_pid-1)] = 1] (0:104:2 - 1)
		IfNotBlocked
		reached[0][94] = 1;
		(trpt+1)->bup.ovals = grab_ints(2);
		(trpt+1)->bup.ovals[0] = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 1;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(104, 96, 104) */
		reached[0][96] = 1;
		;
		/* merge: index = 0(104, 97, 104) */
		reached[0][97] = 1;
		(trpt+1)->bup.ovals[1] = ((int)((P0 *)_this)->index);
		((P0 *)_this)->index = 0;
#ifdef VAR_RANGES
		logval("processor:index", ((int)((P0 *)_this)->index));
#endif
		;
		/* merge: .(goto)(0, 105, 104) */
		reached[0][105] = 1;
		;
		_m = 3; goto P999; /* 3 */
	case 49: // STATE 97 - mesi.pml:135 - [index = 0] (0:104:1 - 3)
		IfNotBlocked
		reached[0][97] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)_this)->index);
		((P0 *)_this)->index = 0;
#ifdef VAR_RANGES
		logval("processor:index", ((int)((P0 *)_this)->index));
#endif
		;
		/* merge: .(goto)(0, 105, 104) */
		reached[0][105] = 1;
		;
		_m = 3; goto P999; /* 1 */
	case 50: // STATE 98 - mesi.pml:138 - [((index<2))] (104:0:3 - 1)
		IfNotBlocked
		reached[0][98] = 1;
		if (!((((int)((P0 *)_this)->index)<2)))
			continue;
		/* merge: ram_index = ((cache_line_indices[(_pid-1)]*2)+index)(104, 99, 104) */
		reached[0][99] = 1;
		(trpt+1)->bup.ovals = grab_ints(3);
		(trpt+1)->bup.ovals[0] = ((int)((P0 *)_this)->ram_index);
		((P0 *)_this)->ram_index = ((((int)now.cache_line_indices[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])*2)+((int)((P0 *)_this)->index));
#ifdef VAR_RANGES
		logval("processor:ram_index", ((int)((P0 *)_this)->ram_index));
#endif
		;
		/* merge: cache[(index+((_pid-1)*2))] = ram[ram_index](104, 100, 104) */
		reached[0][100] = 1;
		(trpt+1)->bup.ovals[1] = ((int)now.cache[ Index((((int)((P0 *)_this)->index)+((((int)((P0 *)_this)->_pid)-1)*2)), 4) ]);
		now.cache[ Index((((P0 *)_this)->index+((((P0 *)_this)->_pid-1)*2)), 4) ] = ((int)now.ram[ Index(((int)((P0 *)_this)->ram_index), 4) ]);
#ifdef VAR_RANGES
		logval("cache[(processor:index+((_pid-1)*2))]", ((int)now.cache[ Index((((int)((P0 *)_this)->index)+((((int)((P0 *)_this)->_pid)-1)*2)), 4) ]));
#endif
		;
		/* merge: index = (index+1)(104, 101, 104) */
		reached[0][101] = 1;
		(trpt+1)->bup.ovals[2] = ((int)((P0 *)_this)->index);
		((P0 *)_this)->index = (((int)((P0 *)_this)->index)+1);
#ifdef VAR_RANGES
		logval("processor:index", ((int)((P0 *)_this)->index));
#endif
		;
		/* merge: .(goto)(0, 105, 104) */
		reached[0][105] = 1;
		;
		_m = 3; goto P999; /* 4 */
	case 51: // STATE 108 - mesi.pml:145 - [(((states[(_pid-1)]==0)&&(type==2)))] (0:0:1 - 1)
		IfNotBlocked
		reached[0][108] = 1;
		if (!(((((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==0)&&(((int)((P0 *)_this)->type)==2))))
			continue;
		if (TstOnly) return 1; /* TT */
		/* dead 1: type */  (trpt+1)->bup.oval = ((P0 *)_this)->type;
#ifdef HAS_CODE
		if (!readtrail)
#endif
			((P0 *)_this)->type = 0;
		_m = 3; goto P999; /* 0 */
	case 52: // STATE 109 - mesi.pml:148 - [((cache_line_indices[(_pid-1)]==cache_line_indices[(other_pid-1)]))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][109] = 1;
		if (!((((int)now.cache_line_indices[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==((int)now.cache_line_indices[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ]))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 53: // STATE 110 - mesi.pml:150 - [((states[(other_pid-1)]==0))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][110] = 1;
		if (!((((int)now.states[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ])==0)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 54: // STATE 112 - mesi.pml:151 - [((states[(other_pid-1)]==1))] (136:0:1 - 1)
		IfNotBlocked
		reached[0][112] = 1;
		if (!((((int)now.states[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ])==1)))
			continue;
		/* merge: buses[(other_pid-1)] = 2(0, 113, 136) */
		reached[0][113] = 1;
		(trpt+1)->bup.oval = ((int)now.buses[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ]);
		now.buses[ Index((((P0 *)_this)->other_pid-1), 2) ] = 2;
#ifdef VAR_RANGES
		logval("buses[(processor:other_pid-1)]", ((int)now.buses[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(0, 129, 136) */
		reached[0][129] = 1;
		;
		/* merge: .(goto)(0, 133, 136) */
		reached[0][133] = 1;
		;
		_m = 3; goto P999; /* 3 */
	case 55: // STATE 114 - mesi.pml:152 - [((states[(other_pid-1)]==2))] (136:0:1 - 1)
		IfNotBlocked
		reached[0][114] = 1;
		if (!((((int)now.states[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ])==2)))
			continue;
		/* merge: buses[(other_pid-1)] = 2(0, 115, 136) */
		reached[0][115] = 1;
		(trpt+1)->bup.oval = ((int)now.buses[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ]);
		now.buses[ Index((((P0 *)_this)->other_pid-1), 2) ] = 2;
#ifdef VAR_RANGES
		logval("buses[(processor:other_pid-1)]", ((int)now.buses[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(0, 129, 136) */
		reached[0][129] = 1;
		;
		/* merge: .(goto)(0, 133, 136) */
		reached[0][133] = 1;
		;
		_m = 3; goto P999; /* 3 */
	case 56: // STATE 116 - mesi.pml:153 - [((states[(other_pid-1)]==3))] (125:0:2 - 1)
		IfNotBlocked
		reached[0][116] = 1;
		if (!((((int)now.states[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ])==3)))
			continue;
		/* merge: buses[(other_pid-1)] = 2(125, 117, 125) */
		reached[0][117] = 1;
		(trpt+1)->bup.ovals = grab_ints(2);
		(trpt+1)->bup.ovals[0] = ((int)now.buses[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ]);
		now.buses[ Index((((P0 *)_this)->other_pid-1), 2) ] = 2;
#ifdef VAR_RANGES
		logval("buses[(processor:other_pid-1)]", ((int)now.buses[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ]));
#endif
		;
		/* merge: index = 0(125, 118, 125) */
		reached[0][118] = 1;
		(trpt+1)->bup.ovals[1] = ((int)((P0 *)_this)->index);
		((P0 *)_this)->index = 0;
#ifdef VAR_RANGES
		logval("processor:index", ((int)((P0 *)_this)->index));
#endif
		;
		/* merge: .(goto)(0, 126, 125) */
		reached[0][126] = 1;
		;
		_m = 3; goto P999; /* 3 */
	case 57: // STATE 119 - mesi.pml:158 - [((index<2))] (125:0:3 - 1)
		IfNotBlocked
		reached[0][119] = 1;
		if (!((((int)((P0 *)_this)->index)<2)))
			continue;
		/* merge: ram_index = ((cache_line_indices[(other_pid-1)]*2)+index)(125, 120, 125) */
		reached[0][120] = 1;
		(trpt+1)->bup.ovals = grab_ints(3);
		(trpt+1)->bup.ovals[0] = ((int)((P0 *)_this)->ram_index);
		((P0 *)_this)->ram_index = ((((int)now.cache_line_indices[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ])*2)+((int)((P0 *)_this)->index));
#ifdef VAR_RANGES
		logval("processor:ram_index", ((int)((P0 *)_this)->ram_index));
#endif
		;
		/* merge: ram[ram_index] = cache[(index+((other_pid-1)*2))](125, 121, 125) */
		reached[0][121] = 1;
		(trpt+1)->bup.ovals[1] = ((int)now.ram[ Index(((int)((P0 *)_this)->ram_index), 4) ]);
		now.ram[ Index(((P0 *)_this)->ram_index, 4) ] = ((int)now.cache[ Index((((int)((P0 *)_this)->index)+((((int)((P0 *)_this)->other_pid)-1)*2)), 4) ]);
#ifdef VAR_RANGES
		logval("ram[processor:ram_index]", ((int)now.ram[ Index(((int)((P0 *)_this)->ram_index), 4) ]));
#endif
		;
		/* merge: index = (index+1)(125, 122, 125) */
		reached[0][122] = 1;
		(trpt+1)->bup.ovals[2] = ((int)((P0 *)_this)->index);
		((P0 *)_this)->index = (((int)((P0 *)_this)->index)+1);
#ifdef VAR_RANGES
		logval("processor:index", ((int)((P0 *)_this)->index));
#endif
		;
		/* merge: .(goto)(0, 126, 125) */
		reached[0][126] = 1;
		;
		_m = 3; goto P999; /* 4 */
	case 58: // STATE 134 - mesi.pml:169 - [cache[cache_index] = 0] (0:174:2 - 1)
		IfNotBlocked
		reached[0][134] = 1;
		(trpt+1)->bup.ovals = grab_ints(2);
		(trpt+1)->bup.ovals[0] = ((int)now.cache[ Index(((int)((P0 *)_this)->cache_index), 4) ]);
		now.cache[ Index(((P0 *)_this)->cache_index, 4) ] = 0;
#ifdef VAR_RANGES
		logval("cache[processor:cache_index]", ((int)now.cache[ Index(((int)((P0 *)_this)->cache_index), 4) ]));
#endif
		;
		/* merge: .(goto)(174, 137, 174) */
		reached[0][137] = 1;
		;
		/* merge: states[(_pid-1)] = 3(174, 138, 174) */
		reached[0][138] = 1;
		(trpt+1)->bup.ovals[1] = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 3;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(0, 173, 174) */
		reached[0][173] = 1;
		;
		/* merge: .(goto)(0, 175, 174) */
		reached[0][175] = 1;
		;
		_m = 3; goto P999; /* 4 */
	case 59: // STATE 137 - mesi.pml:173 - [.(goto)] (0:174:1 - 2)
		IfNotBlocked
		reached[0][137] = 1;
		;
		/* merge: states[(_pid-1)] = 3(174, 138, 174) */
		reached[0][138] = 1;
		(trpt+1)->bup.oval = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 3;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(0, 173, 174) */
		reached[0][173] = 1;
		;
		/* merge: .(goto)(0, 175, 174) */
		reached[0][175] = 1;
		;
		_m = 3; goto P999; /* 3 */
	case 60: // STATE 135 - mesi.pml:170 - [cache[cache_index] = 1] (0:174:2 - 1)
		IfNotBlocked
		reached[0][135] = 1;
		(trpt+1)->bup.ovals = grab_ints(2);
		(trpt+1)->bup.ovals[0] = ((int)now.cache[ Index(((int)((P0 *)_this)->cache_index), 4) ]);
		now.cache[ Index(((P0 *)_this)->cache_index, 4) ] = 1;
#ifdef VAR_RANGES
		logval("cache[processor:cache_index]", ((int)now.cache[ Index(((int)((P0 *)_this)->cache_index), 4) ]));
#endif
		;
		/* merge: .(goto)(174, 137, 174) */
		reached[0][137] = 1;
		;
		/* merge: states[(_pid-1)] = 3(174, 138, 174) */
		reached[0][138] = 1;
		(trpt+1)->bup.ovals[1] = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 3;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(0, 173, 174) */
		reached[0][173] = 1;
		;
		/* merge: .(goto)(0, 175, 174) */
		reached[0][175] = 1;
		;
		_m = 3; goto P999; /* 4 */
	case 61: // STATE 140 - mesi.pml:175 - [(((states[(_pid-1)]==1)&&(type==1)))] (0:0:1 - 1)
		IfNotBlocked
		reached[0][140] = 1;
		if (!(((((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==1)&&(((int)((P0 *)_this)->type)==1))))
			continue;
		if (TstOnly) return 1; /* TT */
		/* dead 1: type */  (trpt+1)->bup.oval = ((P0 *)_this)->type;
#ifdef HAS_CODE
		if (!readtrail)
#endif
			((P0 *)_this)->type = 0;
		_m = 3; goto P999; /* 0 */
	case 62: // STATE 142 - mesi.pml:177 - [(((states[(_pid-1)]==1)&&(type==2)))] (0:0:1 - 1)
		IfNotBlocked
		reached[0][142] = 1;
		if (!(((((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==1)&&(((int)((P0 *)_this)->type)==2))))
			continue;
		if (TstOnly) return 1; /* TT */
		/* dead 1: type */  (trpt+1)->bup.oval = ((P0 *)_this)->type;
#ifdef HAS_CODE
		if (!readtrail)
#endif
			((P0 *)_this)->type = 0;
		_m = 3; goto P999; /* 0 */
	case 63: // STATE 143 - mesi.pml:180 - [cache[cache_index] = 0] (0:174:2 - 1)
		IfNotBlocked
		reached[0][143] = 1;
		(trpt+1)->bup.ovals = grab_ints(2);
		(trpt+1)->bup.ovals[0] = ((int)now.cache[ Index(((int)((P0 *)_this)->cache_index), 4) ]);
		now.cache[ Index(((P0 *)_this)->cache_index, 4) ] = 0;
#ifdef VAR_RANGES
		logval("cache[processor:cache_index]", ((int)now.cache[ Index(((int)((P0 *)_this)->cache_index), 4) ]));
#endif
		;
		/* merge: .(goto)(174, 146, 174) */
		reached[0][146] = 1;
		;
		/* merge: states[(_pid-1)] = 3(174, 147, 174) */
		reached[0][147] = 1;
		(trpt+1)->bup.ovals[1] = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 3;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(0, 173, 174) */
		reached[0][173] = 1;
		;
		/* merge: .(goto)(0, 175, 174) */
		reached[0][175] = 1;
		;
		_m = 3; goto P999; /* 4 */
	case 64: // STATE 146 - mesi.pml:184 - [.(goto)] (0:174:1 - 2)
		IfNotBlocked
		reached[0][146] = 1;
		;
		/* merge: states[(_pid-1)] = 3(174, 147, 174) */
		reached[0][147] = 1;
		(trpt+1)->bup.oval = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 3;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(0, 173, 174) */
		reached[0][173] = 1;
		;
		/* merge: .(goto)(0, 175, 174) */
		reached[0][175] = 1;
		;
		_m = 3; goto P999; /* 3 */
	case 65: // STATE 144 - mesi.pml:181 - [cache[cache_index] = 1] (0:174:2 - 1)
		IfNotBlocked
		reached[0][144] = 1;
		(trpt+1)->bup.ovals = grab_ints(2);
		(trpt+1)->bup.ovals[0] = ((int)now.cache[ Index(((int)((P0 *)_this)->cache_index), 4) ]);
		now.cache[ Index(((P0 *)_this)->cache_index, 4) ] = 1;
#ifdef VAR_RANGES
		logval("cache[processor:cache_index]", ((int)now.cache[ Index(((int)((P0 *)_this)->cache_index), 4) ]));
#endif
		;
		/* merge: .(goto)(174, 146, 174) */
		reached[0][146] = 1;
		;
		/* merge: states[(_pid-1)] = 3(174, 147, 174) */
		reached[0][147] = 1;
		(trpt+1)->bup.ovals[1] = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 3;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(0, 173, 174) */
		reached[0][173] = 1;
		;
		/* merge: .(goto)(0, 175, 174) */
		reached[0][175] = 1;
		;
		_m = 3; goto P999; /* 4 */
	case 66: // STATE 149 - mesi.pml:186 - [(((states[(_pid-1)]==2)&&(type==1)))] (0:0:1 - 1)
		IfNotBlocked
		reached[0][149] = 1;
		if (!(((((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==2)&&(((int)((P0 *)_this)->type)==1))))
			continue;
		if (TstOnly) return 1; /* TT */
		/* dead 1: type */  (trpt+1)->bup.oval = ((P0 *)_this)->type;
#ifdef HAS_CODE
		if (!readtrail)
#endif
			((P0 *)_this)->type = 0;
		_m = 3; goto P999; /* 0 */
	case 67: // STATE 151 - mesi.pml:188 - [(((states[(_pid-1)]==2)&&(type==2)))] (0:0:1 - 1)
		IfNotBlocked
		reached[0][151] = 1;
		if (!(((((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==2)&&(((int)((P0 *)_this)->type)==2))))
			continue;
		if (TstOnly) return 1; /* TT */
		/* dead 1: type */  (trpt+1)->bup.oval = ((P0 *)_this)->type;
#ifdef HAS_CODE
		if (!readtrail)
#endif
			((P0 *)_this)->type = 0;
		_m = 3; goto P999; /* 0 */
	case 68: // STATE 152 - mesi.pml:191 - [((cache_line_indices[(_pid-1)]==cache_line_indices[(other_pid-1)]))] (160:0:1 - 1)
		IfNotBlocked
		reached[0][152] = 1;
		if (!((((int)now.cache_line_indices[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==((int)now.cache_line_indices[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ]))))
			continue;
		/* merge: buses[(other_pid-1)] = 2(0, 153, 160) */
		reached[0][153] = 1;
		(trpt+1)->bup.oval = ((int)now.buses[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ]);
		now.buses[ Index((((P0 *)_this)->other_pid-1), 2) ] = 2;
#ifdef VAR_RANGES
		logval("buses[(processor:other_pid-1)]", ((int)now.buses[ Index((((int)((P0 *)_this)->other_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(0, 157, 160) */
		reached[0][157] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 69: // STATE 158 - mesi.pml:197 - [cache[cache_index] = 0] (0:174:2 - 1)
		IfNotBlocked
		reached[0][158] = 1;
		(trpt+1)->bup.ovals = grab_ints(2);
		(trpt+1)->bup.ovals[0] = ((int)now.cache[ Index(((int)((P0 *)_this)->cache_index), 4) ]);
		now.cache[ Index(((P0 *)_this)->cache_index, 4) ] = 0;
#ifdef VAR_RANGES
		logval("cache[processor:cache_index]", ((int)now.cache[ Index(((int)((P0 *)_this)->cache_index), 4) ]));
#endif
		;
		/* merge: .(goto)(174, 161, 174) */
		reached[0][161] = 1;
		;
		/* merge: states[(_pid-1)] = 3(174, 162, 174) */
		reached[0][162] = 1;
		(trpt+1)->bup.ovals[1] = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 3;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(0, 173, 174) */
		reached[0][173] = 1;
		;
		/* merge: .(goto)(0, 175, 174) */
		reached[0][175] = 1;
		;
		_m = 3; goto P999; /* 4 */
	case 70: // STATE 161 - mesi.pml:201 - [.(goto)] (0:174:1 - 2)
		IfNotBlocked
		reached[0][161] = 1;
		;
		/* merge: states[(_pid-1)] = 3(174, 162, 174) */
		reached[0][162] = 1;
		(trpt+1)->bup.oval = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 3;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(0, 173, 174) */
		reached[0][173] = 1;
		;
		/* merge: .(goto)(0, 175, 174) */
		reached[0][175] = 1;
		;
		_m = 3; goto P999; /* 3 */
	case 71: // STATE 159 - mesi.pml:198 - [cache[cache_index] = 1] (0:174:2 - 1)
		IfNotBlocked
		reached[0][159] = 1;
		(trpt+1)->bup.ovals = grab_ints(2);
		(trpt+1)->bup.ovals[0] = ((int)now.cache[ Index(((int)((P0 *)_this)->cache_index), 4) ]);
		now.cache[ Index(((P0 *)_this)->cache_index, 4) ] = 1;
#ifdef VAR_RANGES
		logval("cache[processor:cache_index]", ((int)now.cache[ Index(((int)((P0 *)_this)->cache_index), 4) ]));
#endif
		;
		/* merge: .(goto)(174, 161, 174) */
		reached[0][161] = 1;
		;
		/* merge: states[(_pid-1)] = 3(174, 162, 174) */
		reached[0][162] = 1;
		(trpt+1)->bup.ovals[1] = ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]);
		now.states[ Index((((P0 *)_this)->_pid-1), 2) ] = 3;
#ifdef VAR_RANGES
		logval("states[(_pid-1)]", ((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ]));
#endif
		;
		/* merge: .(goto)(0, 173, 174) */
		reached[0][173] = 1;
		;
		/* merge: .(goto)(0, 175, 174) */
		reached[0][175] = 1;
		;
		_m = 3; goto P999; /* 4 */
	case 72: // STATE 164 - mesi.pml:203 - [(((states[(_pid-1)]==3)&&(type==1)))] (0:0:1 - 1)
		IfNotBlocked
		reached[0][164] = 1;
		if (!(((((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==3)&&(((int)((P0 *)_this)->type)==1))))
			continue;
		if (TstOnly) return 1; /* TT */
		/* dead 1: type */  (trpt+1)->bup.oval = ((P0 *)_this)->type;
#ifdef HAS_CODE
		if (!readtrail)
#endif
			((P0 *)_this)->type = 0;
		_m = 3; goto P999; /* 0 */
	case 73: // STATE 166 - mesi.pml:205 - [(((states[(_pid-1)]==3)&&(type==2)))] (0:0:1 - 1)
		IfNotBlocked
		reached[0][166] = 1;
		if (!(((((int)now.states[ Index((((int)((P0 *)_this)->_pid)-1), 2) ])==3)&&(((int)((P0 *)_this)->type)==2))))
			continue;
		if (TstOnly) return 1; /* TT */
		/* dead 1: type */  (trpt+1)->bup.oval = ((P0 *)_this)->type;
#ifdef HAS_CODE
		if (!readtrail)
#endif
			((P0 *)_this)->type = 0;
		_m = 3; goto P999; /* 0 */
	case 74: // STATE 167 - mesi.pml:208 - [cache[cache_index] = 0] (0:174:1 - 1)
		IfNotBlocked
		reached[0][167] = 1;
		(trpt+1)->bup.oval = ((int)now.cache[ Index(((int)((P0 *)_this)->cache_index), 4) ]);
		now.cache[ Index(((P0 *)_this)->cache_index, 4) ] = 0;
#ifdef VAR_RANGES
		logval("cache[processor:cache_index]", ((int)now.cache[ Index(((int)((P0 *)_this)->cache_index), 4) ]));
#endif
		;
		/* merge: .(goto)(174, 170, 174) */
		reached[0][170] = 1;
		;
		/* merge: .(goto)(0, 173, 174) */
		reached[0][173] = 1;
		;
		/* merge: .(goto)(0, 175, 174) */
		reached[0][175] = 1;
		;
		_m = 3; goto P999; /* 3 */
	case 75: // STATE 170 - mesi.pml:211 - [.(goto)] (0:174:0 - 2)
		IfNotBlocked
		reached[0][170] = 1;
		;
		/* merge: .(goto)(0, 173, 174) */
		reached[0][173] = 1;
		;
		/* merge: .(goto)(0, 175, 174) */
		reached[0][175] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 76: // STATE 168 - mesi.pml:209 - [cache[cache_index] = 1] (0:174:1 - 1)
		IfNotBlocked
		reached[0][168] = 1;
		(trpt+1)->bup.oval = ((int)now.cache[ Index(((int)((P0 *)_this)->cache_index), 4) ]);
		now.cache[ Index(((P0 *)_this)->cache_index, 4) ] = 1;
#ifdef VAR_RANGES
		logval("cache[processor:cache_index]", ((int)now.cache[ Index(((int)((P0 *)_this)->cache_index), 4) ]));
#endif
		;
		/* merge: .(goto)(174, 170, 174) */
		reached[0][170] = 1;
		;
		/* merge: .(goto)(0, 173, 174) */
		reached[0][173] = 1;
		;
		/* merge: .(goto)(0, 175, 174) */
		reached[0][175] = 1;
		;
		_m = 3; goto P999; /* 3 */
	case 77: // STATE 177 - mesi.pml:214 - [-end-] (0:0:0 - 3)
		IfNotBlocked
		reached[0][177] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */
	case  _T5:	/* np_ */
		if (!((!(trpt->o_pm&4) && !(trpt->tau&128))))
			continue;
		/* else fall through */
	case  _T2:	/* true */
		_m = 3; goto P999;
#undef rand
	}

